console.log('');
console.log('*********************** load script.js ***********************');
console.log('');

// *********************** menu ***********************
const btnBurger = document.getElementById('burger');
const btnNav = document.getElementById('nav');

btnBurger.addEventListener('click', (e) => {
  console.log('btnBurger [click]');

  btnBurger.classList.toggle('header__burger_active');
  btnNav.classList.toggle('navigation__active');
});

// *********************** feedback ***********************
const btnSubmit = document.getElementById('submit');
const elSuccess = document.getElementById('success');
const elError = document.getElementById('error');
const inputName = document.getElementById('name');
const inputEmail = document.getElementById('email');
const textareaMessage = document.getElementById('message');

let timerSuccess;

btnSubmit.addEventListener('click', (e) => {
  console.log('btnSubmit [click]');

  e.preventDefault();
  const fields = [inputName, inputEmail, textareaMessage];

  if (fields.every(el => el.value.trim())) {
    error.classList.remove('form__fields-error_active');
    elSuccess.classList.add('wrapper-success_active');

    fields.forEach((e) => { e.value = '' });

    clearTimeout(timerSuccess);
    timerSuccess = setTimeout(() => { elSuccess.classList.remove('wrapper-success_active'); }, 3000);
  } else {
    error.classList.add('form__fields-error_active');
  }
});